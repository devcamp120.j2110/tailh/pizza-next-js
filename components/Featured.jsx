// Next IMG
import Image from "next/image";
// Style
import styles from "../styles/Featured.module.css";
// Image
import ArrowLeft from "../public/img/arrowl.png";
import ArrowRight from "../public/img/arrowr.png";
import { useState } from "react";

const Featured = () => {
  const [index, setIndex] = useState(0);
  const featuredImages = ["featured.png", "featured2.png", "featured3.png"];

  // Handle
  const handleSlide = (direction) => {
    if (direction === "left") {
      setIndex(index !== 0 ? index - 1 : 2);
    }
    if (direction === "right") {
      setIndex(index !== 2 ? index + 1 : 0);
    }
  };

  return (
    <div className={styles.container}>
      <div
        className={styles.arrowContainer}
        onClick={() => {
          handleSlide("left");
        }}
      >
        <Image src={ArrowLeft} alt="arrow-left" layout="fill"></Image>
      </div>
      <div
        className={styles.wrapper}
        style={{ transform: `translateX(${-100 * index}vw)` }}
      >
        {featuredImages.map((image, index) => {
          return (
            <div className={styles.imgContainer} key={index}>
              <Image
                src={require(`../public/img/${image}`)}
                alt="main-img"
                layout="fill"
                objectFit="cover"
              ></Image>
            </div>
          );
        })}
      </div>
      <div
        className={styles.arrowContainer}
        onClick={() => {
          handleSlide("right");
        }}
      >
        <Image src={ArrowRight} alt="arrow-right" layout="fill"></Image>
      </div>
    </div>
  );
};

export default Featured;
