// Next IMG
import Image from "next/image";
// Style
import styles from "../styles/Navbar.module.css";
// Image
import CallImg from "../public/img/telephone.png";
import Logo from "../public/img/logo.png";
import Cart from "../public/img/cart.png";

const Navbar = () => {
  return (
    <div className={styles.container}>
      <div className={styles.item}>
        <div className={styles.callButton}>
          <Image src={CallImg} alt="call-button-img" width="32" height="32" />
        </div>
        <div className={styles.textWrapper}>
          <div className={styles.text}>ORDER NOW!</div>
          <div className={styles.text}>123 456 789</div>
        </div>
      </div>
      <div className={styles.item}>
        <ul className={styles.list}>
          <li className={styles.listItem}>Homepage</li>
          <li className={styles.listItem}>Products</li>
          <li className={styles.listItem}>Menu</li>
          <Image src={Logo} alt="logo-img" width="160" height="69" />
          <li className={styles.listItem}>Events</li>
          <li className={styles.listItem}>Blog</li>
          <li className={styles.listItem}>Contact</li>
        </ul>
      </div>
      <div className={styles.item}>
        <div className={styles.cart}>
          <Image src={Cart} alt="logo-img" width="30px" height="30px" />
          <div className={styles.counter}>2</div>
        </div>
      </div>
    </div>
  );
};

export default Navbar;
