// Next IMG
import Image from "next/image";
// Style
import styles from "../styles/PizzaCard.module.css";
//Image
import Pizza from "../public/img/pizza.png";

const PizzaCard = () => {
  return (
    <div className={styles.container}>
      <Image src={Pizza} alt="" width="500" height="500" />
      <h1 className={styles.title}>Burrata Parma Ham</h1>
      <span className={styles.price}>$19.90</span>
      <p className={styles.desc}>
        Lorem ipsum dolor sit amet consectetur adipisicing elit.
      </p>
    </div>
  );
};

export default PizzaCard;
